package com.xuxueli.executor.sample.jboot.jobhandler;

import com.sc.job.core.biz.dto.ReturnT;
import com.sc.job.core.handler.IJobHandler;
import com.sc.job.core.log.ScJobLogger;

import java.util.concurrent.TimeUnit;

/**
 * 任务Handler示例（Bean模式）
 *
 * 开发步骤：
 * 1、继承"IJobHandler"：“com.xxl.job.core.handler.IJobHandler”；
 * 2、注册到执行器工厂：在 "JFinalCoreConfig.initXxlJobExecutor" 中手动注册，注解key值对应的是调度中心新建任务的JobHandler属性的值。
 * 3、执行日志：需要通过 "XxlJobLogger.log" 打印执行日志；
 *
 * @author scer 2020-12-19 19:43:36
 */
public class DemoJobHandler extends IJobHandler {

	@Override
	public ReturnT<String> execute(String param) throws Exception {
		ScJobLogger.log("XXL-JOB, Hello World.");

		for (int i = 0; i < 5; i++) {
			ScJobLogger.log("beat at:" + i);
			TimeUnit.SECONDS.sleep(2);
		}
		return SUCCESS;
	}

}
